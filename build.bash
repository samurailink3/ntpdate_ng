#!/bin/bash

version="1.1.0"
GOOS_array=(linux windows freebsd)
GOARCH_array=(amd64 386)

for os in ${GOOS_array[@]}
do
  for arch in ${GOARCH_array[@]}
  do
    if [ "$os" == "windows" ]
    then
      export GOOS=$os
      export GOARCH=$arch
      echo "Building ntpdate_ng $version for $os $arch"
      go build -o ntpdate_ng-$version.$os.$arch.exe
    else
      export GOOS=$os
      export GOARCH=$arch
      echo "Building ntpdate_ng $version for $os $arch"
      go build -o ntpdate_ng-$version.$os.$arch
    fi
  done
done

unset GOOS
unset GOARCH

echo "Builds complete."
