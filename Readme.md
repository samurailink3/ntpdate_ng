# ntpdate_ng

What do you do when you need to automatically correct your system time, but your
network administrator has completely blocked all NTP traffic?

`ntpdate_ng` to the rescue!

It uses HTTPS to collect the `Date` header from Google's front page (by
default), then changes your system time to match (if you've used the `-s` flag).

## Version

This project follows [Semantic Versioning 2.0.0](http://semver.org/spec/v2.0.0.html).

The current version is `1.1.0`

## Supported Operating Systems

Linux, Windows, and FreeBSD only for the moment. Future versions will support more
operating systems.

## Building

To build for your particular platform, run `go build ntpdate_ng.go`.

To build all supported platforms run `./build.bash` to start a cross-platform build. This assumes you have
`bash` installed.

Building is supported for Linux, Windows, and FreeBSD. Future versions will support more
operating systems.

## Usage

Running this command without any flags won't do anything. Either choose to
display the time, set the time, or both. To set the system clock, you will need
administrator permissions.

To display server time: `ntpdate_ng -d`

To set system clock from server time: `ntpdate_ng -s`

**Flags:**

```
-d, -display
    Display the current time given by the server
-s, -set-system-clock
    Set system clock to time returned by the server
-u, -url
    Fetch the 'Date' header from this URL instead of the default (default "https://www.google.com")
-v, -version
    Show the current version and exit
```
