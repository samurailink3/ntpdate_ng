package main

import (
	"log"
	"strconv"
	"time"

	"github.com/VividCortex/w32"
)

// SetSystemClock sets the clock for Windows systems, it takes a time
// type as an argument.
func SetSystemClock(returnedHeaderTime time.Time) {
	// SYSTEMTIME struct definition https://github.com/VividCortex/w32/blob/master/typedef_windows.go#L911
	// How to invoke SYSTEMTIME https://msdn.microsoft.com/en-us/library/windows/desktop/ms724950%28v=vs.85%29.aspx

	// To satisfy the Windows API requirements, we need to convert the abreviated
	// day to a uint16 value.
	dayOfWeekMap := map[string]uint16{
		"Sun": 0,
		"Mon": 1,
		"Tue": 2,
		"Wed": 3,
		"Thu": 4,
		"Fri": 5,
		"Sat": 6,
	}

	// For each piece of the date required by the SYSTEMTIME struct, we'll convert
	// it to a uint64, then to a uint16, so the Windows API can use it.

	year, error := strconv.ParseUint(returnedHeaderTime.Format("2006"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	yearU := uint16(year)

	month, error := strconv.ParseUint(returnedHeaderTime.Format("1"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	monthU := uint16(month)

	dayOfWeek := dayOfWeekMap[returnedHeaderTime.Format("Mon")]

	day, error := strconv.ParseUint(returnedHeaderTime.Format("2"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	dayU := uint16(day)

	hour, error := strconv.ParseUint(returnedHeaderTime.Format("15"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	hourU := uint16(hour)

	minute, error := strconv.ParseUint(returnedHeaderTime.Format("04"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	minuteU := uint16(minute)

	second, error := strconv.ParseUint(returnedHeaderTime.Format("05"), 10, 16)
	if error != nil {
		log.Fatal(error)
	}
	secondU := uint16(second)

	// We then make a new SYSTEMTIME object and pass it to SetSystemTime
	convertedTime := w32.SYSTEMTIME{yearU, monthU, dayOfWeek, dayU, hourU, minuteU, secondU, 0}

	// SetSystemTime works in UTC, then in converted by Windows to the local time zone
	w32.SetSystemTime(&convertedTime)
}
