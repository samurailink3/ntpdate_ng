package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
)

// Version is the version of the application, shown with '-v' or '-version'
var Version = "1.1.0"

// DisplayVersion is a boolean value that allows the user to display the program
// version. If this is true, the version is displayed and the program exits with
// a status of 2.
var DisplayVersion bool

// URL is the URL from which we'll fetch the 'Date' header.
var URL string

// Display is a boolean value on whether or not to display the date from the
// server. This defaults to 'false' as to not create extra noise in cronjobs.
var Display bool

// SetSystemClockFlag is a boolean value that controls whether or not the system
// clock is set to the date returned by the server. This defaults to 'false' as
// we don't want to change anything without explicit permission.
var SetSystemClockFlag bool

// OperatingSystem is the OS reported by GoLang. We use this to determine how to
// set the time.
var OperatingSystem = runtime.GOOS

// Init initializes the program state, including Args and Flags
func Init() {
	const (
		usage                     = "Running this command without any flags won't do anything.\nEither choose to display the time, set the time, or both.\nTo set the system clock, you will need administrator permissions.\n"
		example                   = "To display server time: 'ntpdate_ng -d'\nTo set system clock from server time: 'ntpdate_ng -s'\n"
		defaultDisplay            = false
		helpDisplay               = "Display the current time given by the server, in UTC"
		defaultSetSystemClockFlag = false
		helpSetSystemClockFlag    = "Set system clock to time returned by the server"
		defaultURL                = "https://www.google.com"
		helpURL                   = "Fetch the 'Date' header from this URL instead of the default"
		helpVersion               = "Show the current version and exit"
	)

	flag.BoolVar(&Display, "d", defaultDisplay, helpDisplay)
	flag.BoolVar(&Display, "display", defaultDisplay, helpDisplay)
	flag.BoolVar(&SetSystemClockFlag, "s", defaultSetSystemClockFlag, helpSetSystemClockFlag)
	flag.BoolVar(&SetSystemClockFlag, "set-system-clock", defaultSetSystemClockFlag, helpSetSystemClockFlag)
	flag.StringVar(&URL, "u", defaultURL, helpURL)
	flag.StringVar(&URL, "url", defaultURL, helpURL)
	flag.BoolVar(&DisplayVersion, "v", false, helpVersion)
	flag.BoolVar(&DisplayVersion, "version", false, helpVersion)
	flag.Parse()

	if len(os.Args) < 2 {
		fmt.Println(usage)
		fmt.Println(example)
		flag.Usage()
		os.Exit(2)
	}

	if DisplayVersion {
		fmt.Println(os.Args[0], Version)
		os.Exit(2)
	}
}

func main() {
	Init()

	response, error := http.Get(URL)
	if error != nil {
		log.Fatal(error)
	}

	headerDate, error := http.ParseTime(response.Header["Date"][0])
	if error != nil {
		log.Fatal(error)
	}

	if Display {
		fmt.Println(headerDate)
	}

	if SetSystemClockFlag {
		if OperatingSystem == "linux" || OperatingSystem == "windows" || OperatingSystem == "freebsd" {
			SetSystemClock(headerDate)
		} else {
			log.Fatal(OperatingSystem + " is not supported at this time.")
		}
	}
}
