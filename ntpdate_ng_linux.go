package main

import (
	"log"
	"os"
	"os/exec"
	"time"
)

// SetSystemClock sets the clock for Linux systems, it takes a time type as
// an argument.
func SetSystemClock(returnedHeaderTime time.Time) {
	if os.Getuid() != 0 {
		log.Fatal("You must run this via 'sudo' or as root to change system time.")
	}
	command := exec.Command("date", "-s", returnedHeaderTime.Format(time.UnixDate))
	error := command.Run()
	if error != nil {
		log.Fatal(error)
	}
}
